﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace region
{
    class CompareGlobalJSON
    {
        List<string> FileNames;
        List<RegionNode> currentRegionCodes;
        List<FinallyNode> finallyNodes;
        List<string> updateMessage = new List<string>();
        string initYears;
        string WorkDirectory;
        public CompareGlobalJSON(string directory)
        {
            WorkDirectory = directory;
            FileNames = new List<string>();
            foreach (string file in Directory.GetFiles(directory))
            {
                FileNames.Add(Path.GetFileNameWithoutExtension(file));
            }
            FileNames.Sort((a, b) => Convert.ToInt16(a) - Convert.ToInt16(b));
            InitResource();
            CompareFiles();
            SaveFile(WorkDirectory.Replace("original", "finally"));
        }

        public void InitResource()
        {
            finallyNodes = new List<FinallyNode>();
            initYears = FileNames[0];
            currentRegionCodes = ReadFile(initYears);
            foreach (RegionNode node in currentRegionCodes)
            {
                FinallyNode tempNode = new FinallyNode();
                tempNode.regionName = node.name;
                tempNode.regionCodes = new List<string>();
                tempNode.regionCodes.Add(node.code);
                tempNode.children = new List<FinallyNode>();
                tempNode.updateMessages = new List<string>();
                InitNodes(node, ref tempNode);
                finallyNodes.Add(tempNode);
            }
        }

        private void InitNodes(RegionNode node, ref FinallyNode parent)
        {
            if (node.childrens != null && node.childrens.Count > 0)
            {
                foreach (RegionNode child in node.childrens)
                {
                    FinallyNode tempNode = new FinallyNode();
                    tempNode.regionName = child.name;
                    tempNode.regionCodes = new List<string>();
                    tempNode.regionCodes.Add(child.code);
                    tempNode.children = new List<FinallyNode>();
                    tempNode.updateMessages = new List<string>();
                    parent.children.Add(tempNode);
                    InitNodes(child, ref tempNode);
                }
            }
        }

        public List<RegionNode> ReadFile(string fileName)
        {
            initYears = fileName;
            string filePath = Path.Combine(WorkDirectory, fileName + ".json");
            Console.WriteLine("开始处理文件:" + filePath);
            string content = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<List<RegionNode>>(content);
        }

        public void CompareFiles()
        {
            for (int i = 1; i < FileNames.Count; i++)
            {
                CompareFile(FileNames[i]);
            }
        }

        public void CompareFile(string fileName)
        {
            currentRegionCodes = ReadFile(fileName);
            foreach (RegionNode node in currentRegionCodes)
            {
                CompareForwardNode(node);
            }
        }
        public void CompareForwardNode(RegionNode node)
        {
            DiffForwardNode(node);
            if (node.childrens != null && node.childrens.Count > 0)
            {
                foreach (RegionNode child in node.childrens)
                {
                    CompareForwardNode(child);
                }
            }
        }
        private void DiffForwardNode(RegionNode node)
        {
            if (node.code.EndsWith("0000"))
            {
                int provinceNodeIndex = finallyNodes.FindIndex(el => el.regionCodes.Contains(node.code));
                if (provinceNodeIndex == -1)
                {
                    string message = string.Format("类型[新增] 内容 [{0}年和上一年相比而言：新增节点{1}-{2}]", initYears, node.name, node.code);
                    FinallyNode tempNode = CreateNode(node);
                    tempNode.updateMessages.Add(message);
                    finallyNodes.Add(tempNode);
                    updateMessage.Add(message);
                    Console.WriteLine(message);
                }
                else
                {
                    FinallyNode currentNode = finallyNodes[provinceNodeIndex];
                    if (currentNode.regionName != node.name)
                    {
                        string message = string.Format("类型[更新名称] 内容 [{0}年和上一年相比而言：更新节点{1} 旧名称{2}修改为新名称{3}]", initYears, node.code, currentNode.regionName, node.name);
                        currentNode.regionName = node.name;
                        currentNode.updateMessages.Add(message);
                        updateMessage.Add(message);
                        Console.WriteLine(message);
                    }
                }
            }
            else if (node.code.EndsWith("00"))
            {
                int provinceNodeIndex = finallyNodes.FindIndex(el => el.regionCodes.Contains(node.code.Substring(0, 2) + "0000"));
                if (provinceNodeIndex != -1)
                {
                    FinallyNode provinceNode = finallyNodes[provinceNodeIndex];
                    int areaNodeIndex = provinceNode.children.FindIndex(el => el.regionCodes.Contains(node.code));
                    if (areaNodeIndex != -1)
                    {
                        FinallyNode currentNode = provinceNode.children[areaNodeIndex];
                        if (currentNode.regionName != node.name)
                        {
                            string message = string.Format("类型[更新名称] 内容 [{0}年和上一年相比而言：更新节点{1} 旧名称{2}修改为新名称{3}]", initYears, node.code, currentNode.regionName, node.name);
                            currentNode.regionName = node.name;
                            currentNode.updateMessages.Add(message);
                            updateMessage.Add(message);
                            Console.WriteLine(message);
                        }
                    }
                    else
                    {
                        areaNodeIndex = provinceNode.children.FindIndex(el => el.regionName == node.name);
                        if (areaNodeIndex != -1)
                        {
                            FinallyNode currentNode = provinceNode.children[areaNodeIndex];
                            string message = string.Format("类型[新增编码] 内容 [{0}年和上一年相比而言：更新节点{1} 新增编码{2}]", initYears, currentNode.regionName, node.code);
                            currentNode.regionCodes.Add(node.code);
                            currentNode.updateMessages.Add(message);
                            updateMessage.Add(message);
                            Console.WriteLine(message);
                        }
                        else
                        {
                            string message = string.Format("类型[新增] 内容 [{0}年和上一年相比而言：新增节点{1}-{2}]", initYears, node.name, node.code);
                            FinallyNode tempNode = CreateNode(node);
                            tempNode.updateMessages.Add(message);
                            provinceNode.children.Add(tempNode);
                            updateMessage.Add(message);
                            Console.WriteLine(message);
                        }

                    }
                }
            }
            else
            {
                int provinceNodeIndex = finallyNodes.FindIndex(el => el.regionCodes.Contains(node.code.Substring(0, 2) + "0000"));
                if (provinceNodeIndex != -1)
                {
                    FinallyNode provinceNode = finallyNodes[provinceNodeIndex];
                    int areaNodeIndex = provinceNode.children.FindIndex(el => el.regionCodes.Contains(node.code));
                    if (areaNodeIndex != -1)
                    {
                        FinallyNode areaNode = provinceNode.children[areaNodeIndex];
                        if (areaNode.regionName != node.name)
                        {
                            string message = string.Format("类型[更新名称] 内容 [{0}年和上一年相比而言：更新节点{1} 旧名称{2}修改为新名称{3}]", initYears, node.code, areaNode.regionName, node.name);
                            areaNode.regionName = node.name;
                            areaNode.updateMessages.Add(message);
                            updateMessage.Add(message);
                            Console.WriteLine(message);
                        }
                    }
                    else
                    {
                        areaNodeIndex = provinceNode.children.FindIndex(el => el.regionName == node.name);
                        if (areaNodeIndex != -1)
                        {
                            FinallyNode currentNode = provinceNode.children[areaNodeIndex];
                            string message = string.Format("类型[新增编码] 内容 [{0}年和上一年相比而言：更新节点{1} 新增编码{2}]", initYears, currentNode.regionName, node.code);
                            currentNode.regionCodes.Add(node.code);
                            currentNode.updateMessages.Add(message);
                            updateMessage.Add(message);
                            Console.WriteLine(message);
                        }
                        else
                        {
                            areaNodeIndex = provinceNode.children.FindIndex(el => el.regionCodes.Contains(node.code.Substring(0, 4) + "00"));
                            if (areaNodeIndex != -1)
                            {
                                FinallyNode areaNode = provinceNode.children[areaNodeIndex];
                                int cityNodeIndex = areaNode.children.FindIndex(el => el.regionCodes.Contains(node.code));
                                if (cityNodeIndex != -1)
                                {
                                    FinallyNode cityNode = provinceNode.children[areaNodeIndex].children[cityNodeIndex];
                                    if (cityNode.regionName != node.name)
                                    {
                                        string message = string.Format("类型[更新名称] 内容 [{0}年和上一年相比而言：更新节点{1} 旧名称{2}修改为新名称{3}]", initYears, node.code, cityNode.regionName, node.name);
                                        cityNode.regionName = node.name;
                                        cityNode.updateMessages.Add(message);
                                        updateMessage.Add(message);
                                        Console.WriteLine(message);
                                    }
                                }
                                else
                                {
                                    cityNodeIndex = areaNode.children.FindIndex(el => el.regionName == node.name);
                                    if (cityNodeIndex != -1)
                                    {
                                        FinallyNode cityNode = provinceNode.children[areaNodeIndex].children[cityNodeIndex];
                                        string message = string.Format("类型[新增编码] 内容 [{0}年和上一年相比而言：更新节点{1} 新增编码{2}]", initYears, cityNode.regionName, node.name);
                                        cityNode.regionCodes.Add(node.code);
                                        cityNode.updateMessages.Add(message);
                                        updateMessage.Add(message);
                                        Console.WriteLine(message);
                                    }
                                    else
                                    {
                                        string message = string.Format("类型[新增] 内容 [{0}年和上一年相比而言：新增节点{1}-{2}]", initYears, node.name, node.code);
                                        FinallyNode tempNode = CreateNode(node);
                                        tempNode.updateMessages.Add(message);
                                        areaNode.children.Add(tempNode);
                                        updateMessage.Add(message);
                                        Console.WriteLine(message);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        private FinallyNode CreateNode(RegionNode node)
        {
            FinallyNode tempNode = new FinallyNode();
            tempNode.regionName = node.name;
            tempNode.regionCodes = new List<string>();
            tempNode.regionCodes.Add(node.code);
            tempNode.children = new List<FinallyNode>();
            tempNode.updateMessages = new List<string>();
            return tempNode;
        }

        public void SaveFile(string directory)
        {
            string filePath = Path.Combine(directory, "detail.json");
            Console.WriteLine("保存详细文件：" + filePath);
            string regionStr = JsonConvert.SerializeObject(finallyNodes, Formatting.Indented);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            using (StreamWriter file = File.CreateText(filePath))
            {
                file.Write(regionStr);
                file.Close();
            }
            filePath = Path.Combine(directory, "simplify.json");
            filePath = Path.Combine(directory, "modify.json");
            Console.WriteLine("保存简化文件：" + filePath);
            regionStr = JsonConvert.SerializeObject(updateMessage, Formatting.Indented);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            using (StreamWriter file = File.CreateText(filePath))
            {
                file.Write(regionStr);
                file.Close();
            }
        }

    }
}
