﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace region
{
    class ParseGlobalHTML
    {
        private Queue<RegionNode> regionNodes = new Queue<RegionNode>();

        private List<RegionNode> finallyNodes = new List<RegionNode>();
        public ParseGlobalHTML(string filePath) {
            HtmlDocument doc = new HtmlDocument();
            doc.Load(filePath);
            string content = doc.DocumentNode.InnerText;
            Regex reg = new Regex("(\\d{6})\\s*(\\S+)");
            Match result = reg.Match(content);
            while (result.Success) {
                CreateNode(result);
                result = result.NextMatch();
            }
            FormatTree();
            SaveFile(filePath);
        }

        private void CreateNode(Match match) {
            RegionNode regionNode = new RegionNode();
            regionNode.code = match.Groups[1].Value;
            regionNode.name = match.Groups[2].Value;
            regionNode.childrens = new List<RegionNode>();
            regionNodes.Enqueue(regionNode);
        }

        public void FormatTree() {
            while (regionNodes.Count != 0) {
                RegionNode node = regionNodes.Dequeue();
                if (node.code.EndsWith("0000"))
                {
                    finallyNodes.Add(node);
                }
                else
                {
                    RegionNode tempNode = FindNode(node, finallyNodes);
                    tempNode.childrens.Add(node);
                }

            }
        }

        public RegionNode FindNode(RegionNode node, List<RegionNode> nodes) {
            RegionNode tempNode = searchNodeWithTree(node.code.Substring(0, 4), finallyNodes);
            if (tempNode != null)
            {
                return tempNode;
            }
            else {
                tempNode = searchNodeWithTree(node.code.Substring(0, 2), finallyNodes);
                return tempNode;
            }
        }

        public RegionNode searchNodeWithTree(string code, List<RegionNode> nodes) {
            int i = 2;
            string initCode = code.Substring(0, i);
            RegionNode tempNode = nodes.Find(el => el.code == PaddingCode(initCode));
            RegionNode result = null;
            while (true) {
                if (tempNode != null)
                {
                    if (initCode.Length == code.Length)
                    {
                        result = tempNode;
                        break;
                    }
                    else {
                        i += 2;
                        initCode = code.Substring(0, i);
                        tempNode = tempNode.childrens.Find(el => el.code == PaddingCode(initCode));
                    }
                }
                else {
                    break;
                }
            }
            return result;
        }

        private string PaddingCode(string code, int size = 6) {
            int repeatSize = size - code.Length;
            string result = code + "";
            for (int i = 0; i < repeatSize; i++) {
                result += "0";
            }
            return result;
        }

        private void SaveFile(string filePath) {
            string regionStr = JsonConvert.SerializeObject(finallyNodes, Formatting.Indented);
            string newFile = filePath.Replace(".html", ".json");
            if (File.Exists(newFile))
            {
                File.Delete(newFile);
            }
            using (StreamWriter file = File.CreateText(newFile)) {
                file.Write(regionStr);
            }
        }
    }
}
