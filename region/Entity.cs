﻿using System;
using System.Collections.Generic;
using System.Text;

namespace region
{
    class RegionNode {
        public string name { get; set; }

        public string code { get; set; }

        public List<RegionNode> childrens { get; set; }
    }

    class FinallyNode
    {
        public string regionName { get; set; }

        public List<string> regionCodes { get; set; }

        public List<FinallyNode> children { get; set; }

        public List<string> updateMessages { get; set; }
    }
}
