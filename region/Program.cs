﻿using System;
using System.IO;

namespace region
{
    class Program
    {
        string workDirectory;
        static void Main(string[] args)
        {
            new Program();
        }

        public Program() {
            Console.WriteLine("开始执行程序");
            workDirectory = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, "../../../../"));
            Console.WriteLine("获取当前的工作目录为:" + workDirectory);
            ParseGlobal();
            CompareGlobal();
            Console.ReadKey();
        }

        public void ParseGlobal() {
            Console.WriteLine("开始处理全国的行政区域信息");
            string cmd = Path.Combine(workDirectory, "global/original");
            foreach (string file in Directory.GetFiles(cmd))
            {
                string year = Path.GetFileNameWithoutExtension(file);
                string extension = Path.GetExtension(file);
                if (extension.ToLower() == ".html") {
                    Console.WriteLine(string.Format("开始处理{0}年全国的行政区域信息", year));
                    new ParseGlobalHTML(file);
                }
            }
        }

        public void CompareGlobal() {
            Console.WriteLine("开始比较全国的行政区域信息");
            string cmd = Path.Combine(workDirectory, "global/original");
            new CompareGlobalJSON(cmd);
        }
    }
}
